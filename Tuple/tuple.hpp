#ifndef TUPLE_HPP
#define TUPLE_HPP

#include <typeinfo>
#include <typeindex>
#include <type_traits>

#include "getter.h"

namespace JFL {

template<typename T, typename Head, typename... Tail> struct IsThere;
template< typename, typename... > struct isEqual;
template<typename T1, typename T2> struct compare;
template<typename Head, typename... Tail> struct Last;
template<typename Head, typename... Tail> struct Sz;


template <typename... Types> struct Tuple {};
template <> class Tuple<> {
public:
    typedef std::nullptr_t HeadType;
    typedef Tuple<> TailType;
    const HeadType& first() const {
        return nullptr;
    }
};


template <typename Head, typename... Tail> struct Tuple<Head, Tail...> : Tuple<Tail...> {
public:
    typedef Head HeadType;
    typedef Tuple<Tail...> TailType;

    explicit Tuple(const Tail& ...tail) : Tuple<Tail...>(tail...) {;}
    explicit Tuple(const Head &h, const Tail&... tail) : Tuple<Tail...>(tail...), first_(h) {;}

    //Tuple<Head, Tail...>& operator =(const Tuple<Head, Tail...>& copy);
    template<typename Head1, typename... Tail1> bool operator ==(const Tuple<Head1, Tail1...>& copy) const;

    size_t size() const;
    const Head& first() const;
    const typename Last< Head, Tail... >::next& last() const;
    template<typename T> bool is_there(const T& val) const;

private:
    Head first_;
};


template <typename Head, typename... Tail> template <typename Head1, typename... Tail1>
    bool Tuple<Head, Tail...>::operator ==(const Tuple<Head1, Tail1...>& copy) const
{
    bool is_sz = size() == copy.size();
    if(!is_sz)
        return false;
    return isEqual< Head, Tuple<Head, Tail...>, Head1, Tuple<Head1, Tail1...> >::check(*this, copy);
}


template <typename Head, typename... Tail> inline size_t Tuple<Head, Tail...>::size() const
{
    return Sz<Head, Tail...>::size;
}

template <typename Head, typename... Tail> inline const Head& Tuple<Head, Tail...>::first() const
{
    return first_;
}


template <typename Head, typename... Tail> inline const typename Last< Head, Tail... >::next& Tuple<Head, Tail...>::last() const
{
    return Last< Head, Tail... >::get_last(*this);
}


template <typename Head, typename... Tail> template <typename T> bool Tuple<Head, Tail...>::is_there(const T& val) const
{
    return IsThere<T, Head, Tail...>::check(*this, val);
}


/***********************************************************************************/


template<typename Head1, template<class ...> class Tail1, class ...TArgs,
         typename Head2, template<class ...> class Tail2, class ...UArgs>
struct isEqual<Head1, Tail1<TArgs...>, Head2, Tail2<UArgs...> >
{
    typedef typename Tail1<TArgs...>::HeadType THead;
    typedef typename Tail2<UArgs...>::HeadType UHead;

    typedef typename Tail1<TArgs...>::TailType TTail;
    typedef typename Tail2<UArgs...>::TailType UTail;

    static bool check(const Tail1<TArgs...>& tuple1, const Tail2<UArgs...>& tuple2)
    {
        if(!compare< THead, UHead >::val(tuple1.first(), tuple2.first()))
            return false;

        return isEqual< THead, TTail, UHead, UTail >::check(tuple1, tuple2);
    }
};


template<template<class ...> class Tail1, template<class ...> class Tail2>
struct isEqual<std::nullptr_t, Tail1<>, std::nullptr_t, Tail2<> >
{
    static bool check(const Tail1<>& tuple1, const Tail2<>& tuple2)
    {
        return false;
    }
};

template<typename Head1, template<class ...> class Tail1, template<class ...> class Tail2>
struct isEqual<Head1, Tail1<>, std::nullptr_t, Tail2<> >
{
    static bool check(const Tail1<>& tuple1, const Tail2<>& tuple2)
    {
        return false;
    }
};

template<template<class ...> class Tail1, typename Head2, template<class ...> class Tail2>
struct isEqual<std::nullptr_t, Tail1<>, Head2, Tail2<> >
{
    static bool check(const Tail1<>& tuple1, const Tail2<>& tuple2)
    {
        return false;
    }
};


template<typename Head1, template<class ...> class Tail1,
         typename Head2, template<class ...> class Tail2>
struct isEqual<Head1, Tail1<>, Head2, Tail2<> >
{
    static bool check(const Tail1<Head1>& tuple1, const Tail2<Head2>& tuple2)
    {
        return compare<Head1, Head2>::val(tuple1.first(), tuple2.first());
    }
};



template<typename T1, typename T2> struct compare
{
    static bool val(const T1& t1, const T2& t2)
    {
        return false;
    }

    static bool val(std::nullptr_t p1, std::nullptr_t p2)
    {
        return false;
    }
};


template<typename T1> struct compare<T1, T1>
{
    static bool val(const T1& t1, const T1& t2)
    {
        return t1 == t2;
    }
};


template<typename T, typename Head, typename... Tail> struct IsThere
{
    typedef typename IsThere< T, Tail... >::next next;

    static bool check(const Tuple<Head, Tail...>& tuple, const T& t)
    {
        if(compare<T, Head>::val(t, tuple.first()))
            return true;
        return IsThere<T, Tail...>::check(tuple, t);
    }
};

template<typename T, typename Head> struct IsThere<T, Head>
{
    typedef typename Tuple<Head>::HeadType next;

    static bool check(const Tuple<Head>& tuple, const T& t)
    {
        return compare<T, Head>::val(t, tuple.first());
    }
};




template<typename Head, typename... Tail> struct Last
{
    typedef typename Last< Tail...>::next next;

    static next get_last(const Tuple<Head, Tail...>& t)
    {
        return Last<Tail...>::get_last(t);
    }
};

template<typename Head> struct Last<Head>
{
    typedef typename Tuple<Head>::HeadType next;

    static next get_last(const Tuple<Head>& t)
    {
        return t.first();
    }
};




template <class H, class... Tail> struct Sz
{
    enum {size = 1+Sz<Tail...>::size };
};

template <class Head> struct Sz<Head>
{
    enum {size = 1};
};

}

#endif // TUPLE_HPP
