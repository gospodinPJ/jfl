#ifndef GETTER_H
#define GETTER_H

namespace JFL {

template <typename... Types> struct Tuple;


template<int n, typename Head, typename... Tail> struct Getter
{
    typedef typename Getter< n-1, Tail...>::next_head next_head;

    static next_head get(const Tuple<Head, Tail...>& tuple)
    {
        return Getter< n-1, Tail... >::get(tuple);
    }
};

template<typename Head, typename... Tail> struct Getter<0, Head, Tail...>
{
    typedef typename Tuple<Head, Tail...>::HeadType next_head;

    static next_head get(const Tuple<Head, Tail...>& tuple)
    {
        return tuple.first();
    }
};


template<int n, typename Head, typename... Tail> typename Getter<n, Head, Tail...>::next_head get(const Tuple<Head, Tail...>& tuple)
{
    return Getter<n, Head, Tail...>::get(tuple);
}

}

#endif // GETTER_H
