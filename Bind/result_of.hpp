#ifndef RESULT_OF_HPP
#define RESULT_OF_HPP

namespace JFL
{

template<typename A> struct result_of;


template<typename R> struct result_of< R(*)(void) >
{
    typedef R type;
};


template<typename R, typename T> struct result_of< R(*)(T) >
{
    typedef R type;
};


template<typename R, typename T1, typename T2> struct result_of< R(*)(T1, T2) >
{
    typedef R type;
};


template<typename R, typename T1, typename T2, typename T3> struct result_of< R(*)(T1, T2, T3) >
{
    typedef R type;
};

}

#endif // RESULT_OF_HPP
