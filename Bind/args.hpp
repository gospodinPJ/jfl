#ifndef ARGS_HPP
#define ARGS_HPP

#include "result_of.hpp"

namespace JFL {

template<typename F> class Storage0
{
public:
    typedef typename JFL::result_of<F>::type return_type;

    Storage0(F f) : func_(f) {;}

    return_type operator()()
    {
        return func_();
    }

    template<typename T1> return_type operator()(T1& t1)
    {
        return func_();
    }

    template<typename T1, typename T2> return_type operator()(T1& t1, T2& t2)
    {
        func_();
    }

    template<typename T1, typename T2, typename T3> return_type operator()(T1& t1, T2& t2, T3& t3)
    {
        func_();
    }

    F func_;
};


template<typename F, typename P> class Storage1
{
public:
    typedef typename JFL::result_of<F>::type return_type;

    Storage1(F f, P& p) : func_(f), param_(p) {;}

    return_type operator()()
    {
        return func_(param_());
    }

    template<typename T1> return_type operator()(T1& t1)
    {
        return func_(param_(t1));
    }

    template<typename T1, typename T2> return_type operator()(T1& t1, T2& t2)
    {
        return func_(param_(t1, t2));
    }

    template<typename T1, typename T2, typename T3> return_type operator()(T1& t1, T2& t2, T3& t3)
    {
        return func_(param_(t1, t2, t3));
    }


    F func_;
    P param_;
};


template<typename F, typename P1, typename P2> class Storage2
{
public:
    typedef typename JFL::result_of<F>::type return_type;

    Storage2(F f, P1& p1, P2& p2) : func_(f), param1_(p1), param2_(p2) {;}

    void operator()()
    {
        return func_(param1_(), param2_());
    }

    template<typename T1> return_type operator()(T1& t1)
    {
        return func_(param_1(t1), param2_(t1));
    }

    template<typename T1, typename T2> return_type operator()(T1& t1, T2& t2)
    {
        return func_(param1_(t1, t2), param2_(t1, t2));
    }

    template<typename T1, typename T2, typename T3> return_type operator()(T1& t1, T2& t2, T3& t3)
    {
        return func_(param1_(t1, t2, t3), param2_(t1, t2, t3));
    }

    F func_;
    P1 param1_;
    P2 param2_;
};


template<typename F, typename P1, typename P2, typename P3> class Storage3
{
public:
    typedef typename JFL::result_of<F>::type return_type;

    Storage3(F f, P1& p1, P2& p2, P3& p3) : func_(f), param1_(p1), param2_(p2), param3_(p3) {;}

    return_type operator()()
    {
        return func_(param1_(), param2_(), param3_());
    }

    template<typename T1> return_type operator()(T1& t1)
    {
        return func_(param1_(t1), param2_(t1), param3_(t1));
    }

    template<typename T1, typename T2> return_type operator()(T1& t1, T2& t2)
    {
        return func_(param1_(t1, t2), param2_(t1, t2), param3_(t1, t2));
    }

    template<typename T1, typename T2, typename T3> return_type operator()(T1& t1, T2& t2, T3& t3)
    {
        return func_(param1_(t1, t2, t3), param2_(t1, t2, t3), param3_(t1, t2, t3));
    }

    F func_;
    P1 param1_;
    P2 param2_;
    P3 param3_;
};


}

#endif // ARGS_HPP
