template<int> struct Arg { };


template<typename T> struct Placeholder
{
    Placeholder(T& obj) : t_(obj) {;}

    T& operator()() { return t_; }
    template<typename T1> T& operator()(T1& t1) { return t_; }
    template<typename T1, typename T2> T& operator()(T1& t1, T2& t2) { return t_; }
    template<typename T1, typename T2, typename T3> T& operator()(T1& t1, T2& t2, T3& t3) { return t_; }

    T t_;
};


template<> struct Placeholder< Arg<1> >
{
    Placeholder(Arg<1>& obj) : t_(obj) {;}

    template<typename T1> T1 operator()(T1& t1) { return t1; }
    template<typename T1, typename T2> T1 operator()(T1& t1, T2& t2) { return t1; }
    template<typename T1, typename T2, typename T3> T1 operator()(T1& t1, T2& t2, T3& t3) { return t1; }

    Arg<1> t_;
};

template<> struct Placeholder< Arg<2> >
{
    Placeholder(Arg<2>& obj) : t_(obj) {;}

    //template<typename T1> T1 operator()(T1 t1) { return t1; }
    template<typename T1, typename T2> T2& operator()(T1& t1, T2& t2) { return t2; }
    template<typename T1, typename T2, typename T3> T2& operator()(T1& t1, T2& t2, T3& t3) { return t2; }

    Arg<2> t_;
};

template<> struct Placeholder< Arg<3> >
{
    Placeholder(Arg<3>& obj) : t_(obj) {;}

    //template<typename T1> T1 operator()(T1 t1) { return t1; }
    //template<typename T1, typename T2> T2 operator()(T1 t1, T2 t2) { return t2; }
    template<typename T1, typename T2, typename T3> T3& operator()(T1& t1, T2& t2, T3& t3) { return t3; }

    Arg<3> t_;
};


Arg<1> _1;
Arg<2> _2;
Arg<3> _3;
