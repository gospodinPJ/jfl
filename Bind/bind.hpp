#ifndef BIND_HPP
#define BIND_HPP

#include "args.hpp"


namespace JFL
{

template<class L> class Bind
{
public:
    typedef typename L::return_type result;

    Bind(L arg) : arg_(arg) {;}


    result operator()()
    {
         return arg_();
    }

    template<typename P> result operator()(P p)
    {
        return arg_(p);
    }

    template<typename P1, typename P2> result operator()(P1 p1, P2 p2)
    {
        return arg_(p1, p2);
    }

    template<typename P1, typename P2, typename P3> result operator()(P1 p1, P2 p2, P3 p3)
    {
        return arg_(p1, p2, p3);
    }

private:
    L arg_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


template<typename F> Bind<Storage0<F> > bind(F func)
{
    //we can apply std::decltype for getting return type of func
    //but I choose pattern "Result Of".
    //Hello, Alexandrescu!
    Storage0<F> strg(func);
    return Bind<Storage0<F> >(strg);
}


template<typename F, typename T1> Bind<Storage1<F, Placeholder<T1> > > bind(F func, T1 t1)
{
    Placeholder<T1> ph(t1);
    Storage1<F, Placeholder<T1> > strg(func, ph);

    return Bind<Storage1<F, Placeholder<T1> > >(strg);
}


template<typename F, typename T1, typename T2> Bind<Storage2<F, Placeholder<T1>, Placeholder<T2> > > bind(F func, T1 t1, T2 t2)
{
    Placeholder<T1> ph1(t1);
    Placeholder<T2> ph2(t2);
    Storage2<F, Placeholder<T1>, Placeholder<T2> > strg(func, ph1, ph2);

    return Bind<Storage2<F, Placeholder<T1>, Placeholder<T2> > >(strg);
}

template<typename F, typename T1, typename T2, typename T3>
    Bind<Storage3<F, Placeholder<T1>, Placeholder<T2>, Placeholder<T3> > > bind(F func, T1 t1, T2 t2, T3 t3)
{
    Placeholder<T1> ph1(t1);
    Placeholder<T2> ph2(t2);
    Placeholder<T3> ph3(t3);
    Storage3<F, Placeholder<T1>, Placeholder<T2>, Placeholder<T3> > strg(func, ph1, ph2, ph3);

    return Bind<Storage3<F, Placeholder<T1>, Placeholder<T2>, Placeholder<T3> > >(strg);
}


}

#endif // BIND_HPP
