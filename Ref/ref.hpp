#ifndef REF_HPP
#define REF_HPP

namespace JFL {

template<typename R> class Wrapper_Ref
{
public:
    Wrapper_Ref(R& obj): data_(obj) {;}
    operator R& () const { return data_; }

private:
    R& data_;
};


template<typename R> Wrapper_Ref<R> ref(R& r)
{
    return Wrapper_Ref<R>(r);
}

}


#endif // REF_HPP
