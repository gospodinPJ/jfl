Little C++ library that imitates some features from C++11.
Just For Lulz.

**JFL::Bind** is a dissection of boost::bind (it doesn't yet support the pointers to member functions).

**JFL::Ref** is boost::ref's analog. It provides the wrapper object for passing references.

**JFL::Tuple** describes a tuple type in metaprogramming style.